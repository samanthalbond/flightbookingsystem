package uflybookingsystem;

import java.util.ArrayList;

public class ImportResult {

    //variables to store count of rows
    private static int totalRows, importedRows, failedRows;
    //ArrayList to store error messages to later display to user
    public static ArrayList<String> errorMessages = new ArrayList<>();
    
    public ImportResult(){
        totalRows = 0;
        importedRows = 0;
        failedRows = 0;
        errorMessages.clear();
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getImportedRows() {
        return importedRows;
    }

    public void setImportedRows(int importedRows) {
        this.importedRows = importedRows;
    }

    public int getFailedRows() {
        return failedRows;
    }

    public void setFailedRows(int failedRows) {
        this.failedRows = failedRows;
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }
}
