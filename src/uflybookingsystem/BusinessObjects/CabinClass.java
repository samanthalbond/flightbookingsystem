package uflybookingsystem.BusinessObjects;
//enum class to store CabinClass values
public enum CabinClass {
    
    ECONOMY, PRESTIGE, FIRST
    
}
