package uflybookingsystem.BusinessObjects;


public class Location {

    //variables to store information about Location objects
    private String city;
    private String airportCode;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }
    
    //overridden toString() to display the city and airport code for use on Booking form
    public String toString(){
        return city + " " + airportCode;
    }
}
