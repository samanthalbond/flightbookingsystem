package uflybookingsystem.BusinessObjects;

//import java.text.SimpleDateFormat;
import java.util.*;
import uflybookingsystem.FlightImporter;


public class Flight {

    //variables to store information about Flight objects
    private String flightNumber;
    private String departureAirport;
    private String destinationAirport;
    private double price;
    private Date dateTime;
    private String plane;
    private int seatsTaken;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlane() {
        return plane;
    }

    public void setPlane(String plane) {
        this.plane = plane;
    }

    public int getSeatsTaken() {
        return seatsTaken;
    }

    public void setSeatsTaken(int seatsTaken) {
        this.seatsTaken = seatsTaken;
    }
    
    //overridden toString() to return the flight time in the specified way
    @Override
    public String toString(){
        return FlightImporter.formatter.format(dateTime);
    }  
}
