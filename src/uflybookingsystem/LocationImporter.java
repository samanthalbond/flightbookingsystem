package uflybookingsystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import uflybookingsystem.BusinessObjects.Location;

public class LocationImporter extends BaseImporter {

    //variables to store data from selected file, and to store the results of importing
    private String fileData;
    public static ImportResult results;

    public LocationImporter(String fileName){
        super(fileName);
    }
    
    @Override
    public void run() {
        results = new ImportResult();
        File file = new File(fileName);
        try(FileReader reader = new FileReader(file)){
            int ch = 0;
            while ((ch = reader.read()) != -1){
                fileData += (char)ch;
            }
            String[] lines = fileData.replace("\r\n", "\n").replace("\r", "\n").split("\n");
            String firstLine = lines[0];
            String[] columns = firstLine.split(",");
            if (columns.length == 2){
                if (columns[0].equals("City") && columns[1].equals("Airport Code")){
                    lines[0] = "";
                }
            }
            //used for displaying line number and for use in error messages
            int lineNum = 0; 
            //isFirst used to test if first line in lines array is the first, and if so skips it
            boolean isFirst = true;
            for (String line: lines){
                try{
                    if (isFirst){
                        isFirst = false;
                        continue;
                    }
                    else{
                        if (line.equals("")){
                        continue;  
                    }
                    results.setTotalRows(lineNum);
                    columns = line.split(",");
                    if (columns.length != 2){
                        results.setFailedRows(results.getFailedRows()+1);
                        ImportResult.errorMessages.add("Wrong amount of columns found. Error occurred: Line " + lineNum);
                        continue;
                    }
                    if (columns[0].equals("")){
                        results.setFailedRows(results.getFailedRows()+1); 
                        ImportResult.errorMessages.add("'City' column is empty. Error occured Line " + lineNum);
                        continue;
                    }
                    if (columns[1].equals("")){
                        results.setFailedRows(results.getFailedRows()+1); 
                        ImportResult.errorMessages.add("'Airport Code' column is empty. Error occured Line " + lineNum);
                        continue;
                    } 
                    if (!columns[1].matches("\\w{3}")){
                        results.setFailedRows(results.getFailedRows()+1);
                        ImportResult.errorMessages.add("'Airport Code' doesn't contain three letters. Error occured Line " + lineNum);
                        continue;
                    }
                    //check to see if locationToUpdate already exists in database, if so update row,
                    //if not create new row in database
                    Location locationToUpdate = DatabaseOperations.getLocationByAirportCode(columns[1]);
                    if (locationToUpdate == null){
                        Location locationToAdd = new Location();
                        locationToAdd.setCity(columns[0]);
                        locationToAdd.setAirportCode(columns[1]);
                        DatabaseOperations.AddLocation(locationToAdd);
                        BookingForm.locationsList.add(locationToAdd);
                    }
                    else{
                        locationToUpdate.setCity(columns[0]);
                        DatabaseOperations.UpdateLocation(locationToUpdate);
                    }
                    results.setImportedRows(results.getImportedRows() + 1);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally{
                    lineNum++;
                }
            }  
        }
        catch(IOException ioe){
            ImportResult.errorMessages.add("I/O operation failed: " + ioe.toString());
        }
        catch(Exception e){
            ImportResult.errorMessages.add("Unknown error occured");
        }
    }

}
