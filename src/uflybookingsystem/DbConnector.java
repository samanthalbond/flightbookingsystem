package uflybookingsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {

    //method to allow applications classes to achieve a connection to the database
    public static Connection connectToDb() throws SQLException{
        String url = "jdbc:mysql://localhost:3306/";
        String dataBase = "ufly";
        String username = "root";
        String password = "password";
        return DriverManager.getConnection(url + dataBase, username, password);
    } 
}
