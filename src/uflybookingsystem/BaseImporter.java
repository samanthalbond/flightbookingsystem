package uflybookingsystem;


public abstract class BaseImporter implements Runnable {

    //fileName variable for use with I/O in other classes, and encapsulated results variable 
    //of type ImportResult for use in storing the results of importing data
    public String fileName;
    private ImportResult results;
    
    public BaseImporter(String fileName){
        this.fileName = fileName;
    }
    
    public abstract void run();

    public ImportResult getResults() {
        return results;
    }

    public void setResults(ImportResult results) {
        this.results = results;
    }
    
}
