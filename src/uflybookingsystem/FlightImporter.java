package uflybookingsystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import uflybookingsystem.BusinessObjects.Flight;
import uflybookingsystem.BusinessObjects.Plane;

public class FlightImporter extends BaseImporter {

    //setting up date/time format
    public static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    //variables to store data from given file, and results of the import
    private String fileData;
    public static ImportResult results;
    
    public FlightImporter(String fileName){
        super(fileName);
    }
    
    @Override
    public void run() {
        results = new ImportResult();
        File file = new File(fileName);
        try(FileReader reader = new FileReader(file)){
            int ch = 0;
            while ((ch = reader.read()) != -1){
                fileData += (char)ch;
            }
            String[] lines = fileData.replace("\r\n", "\n").replace("\r", "\n").split("\n");
            String firstLine = lines[0];
            String[] columns = firstLine.split(",");
            if (columns.length == 7){
                if (columns[0].equals("Flight Number") && columns[1].equals("Departure Airport")
                    && columns[2].equals("Destination Airport") && columns[3].equals("Price") && 
                    columns[4].equals("DateTime") && columns[5].equals("Plane") && columns[6].equals("Seats Taken")){
                        lines[0] = ""; 
                }
            }
            //used for displaying line number and for use in error messages
            int lineNum = 0; 
            //isFirst used to test if first line in lines array is the first, and if so skips it
            boolean isFirst = true;
            for (String line: lines){
                try{
                    if (isFirst){
                        isFirst = false;
                    }
                    else{
                        if (line.equals("")){
                        continue;  
                    }
                    results.setTotalRows(lineNum);
                    columns = line.split(",");
                    if (columns.length != 7){
                        results.setFailedRows(results.getFailedRows()+1);
                        ImportResult.errorMessages.add("Wrong amount of columns found. Error occurred: Line " + lineNum);
                        continue;
                    }
                    if (columns[0].equals("") || columns[1].equals("") || columns[2].equals("") || columns[5].equals("")){
                        results.setFailedRows(results.getFailedRows()+1); 
                        ImportResult.errorMessages.add("Either 'Flight Number', 'Departure Airport',"
                                + " 'Destination Airport, or 'Plane' column is empty. Error occured Line " + lineNum);
                        continue;
                    }
                    if (!columns[0].matches("\\w{2}\\d{3}")){
                        results.setFailedRows(results.getFailedRows()+1);
                        ImportResult.errorMessages.add("'Flight Number' doesn't contain correct format. Error occured Line " + lineNum);
                        continue;
                    }
                    if (!columns[1].matches("\\w{3}") || !columns[2].matches("\\w{3}")){
                        results.setFailedRows(results.getFailedRows()+1);
                        ImportResult.errorMessages.add("Either 'Departure Airport' or 'Destination Airport' is not formatted correctly. Error occured Line " + lineNum);
                        continue;
                    }
                    //cast columns and check to ensure they are of the correct type
                    Double price = Double.parseDouble(columns[3]);
                    Integer seatsTaken = Integer.parseInt(columns[6]);
                    Date dateTime = formatter.parse(columns[4]);
                    if (!(price instanceof Double  && seatsTaken instanceof Integer && dateTime instanceof Date)){
                        results.setFailedRows(results.getFailedRows()+1); 
                        ImportResult.errorMessages.add("Either 'Price', 'SeatsTaken', or 'DateTime' column is of the wrong type. Error occured Line " + lineNum);
                        continue;
                    }
                    //check and assign plane type
                    Plane planeType = Plane.AIRBUSA280;
                    for(Plane plane: Plane.values()){
                        if (columns[5].equals(plane.toString())){
                            planeType = plane;
                        }    
                    }
                    if (!columns[5].equals(planeType.toString())){
                        results.setFailedRows(results.getFailedRows() + 1);
                        ImportResult.errorMessages.add("Match not found for plane. Error occured Line " + lineNum);
                        continue;
                    }
                    if (seatsTaken > planeType.getPassengerCapacity()){
                        results.setFailedRows(results.getFailedRows() + 1);
                        ImportResult.errorMessages.add("The amount of Seats Taken exceeds the passenger capacity of the Plane. Error occured Line " + lineNum);
                        continue;
                    }
                    //check to see if flightToUpdate already exists in database, if so update row,
                    //if not create new row in database
                    Flight flightToUpdate = DatabaseOperations.getFlightByFlightNumber(columns[0]);
                    if (flightToUpdate.getFlightNumber() == null && flightToUpdate.getDepartureAirport() == null){ //fix this
                        Flight flightToAdd = new Flight();
                        flightToAdd.setFlightNumber(columns[0]);
                        flightToAdd.setDepartureAirport(columns[1]);
                        flightToAdd.setDestinationAirport(columns[2]);
                        flightToAdd.setPrice(price);
                        flightToAdd.setDateTime(dateTime);
                        flightToAdd.setPlane(planeType.toString());
                        flightToAdd.setSeatsTaken(seatsTaken);
                        DatabaseOperations.AddFlight(flightToAdd);
                    }
                    else{
                        flightToUpdate.setFlightNumber(columns[0]);
                        flightToUpdate.setDepartureAirport(columns[1]);
                        flightToUpdate.setDestinationAirport(columns[2]);
                        flightToUpdate.setPrice(price);
                        flightToUpdate.setDateTime(dateTime);
                        flightToUpdate.setPlane(planeType.toString());
                        flightToUpdate.setSeatsTaken(seatsTaken);
                        DatabaseOperations.UpdateFlight(flightToUpdate);
                    }
                    results.setImportedRows(results.getImportedRows() + 1);
                    }
                }
                catch (NumberFormatException | ParseException e){
                    e.printStackTrace();
                }
                finally{
                    lineNum++;
                }
            }  
        }
        catch(IOException ioe){
            ImportResult.errorMessages.add("I/O operation failed: " + ioe.toString());
        }
        catch(Exception e){
            ImportResult.errorMessages.add("Unknown error occured");
        }
    }
}
